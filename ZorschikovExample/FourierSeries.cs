﻿using mathlib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Math;

namespace ZorschikovExample
{
    public static class FourierSeries
    {
        private static double I1_integral(int k)
        {
            return Integrals.Trapezoid(t => Sqrt(2) * Cos(t / 2) * Sin((k + 1) * t) * Sin(t), PI / 2, PI, 10000);
        }

        private static double I1_opt(int k)
        {
            var minusOnePowerK = k % 2 == 0 ? 1 : -1;
            var I1 = (minusOnePowerK * (1d / (2 * k + 1) - 1d / (2 * k - 1) - 1d / (2 * k + 5) + 1d / (2 * k + 3))
                - Sin((2 * k + 1) * PI / 4) / (2 * k + 1) 
                - Sin((2 * k - 1) * PI / 4) / (2 * k - 1) 
                + Sin((2 * k + 3) * PI / 4) / (2 * k + 3) 
                + Sin((2 * k + 5) * PI / 4) / (2 * k + 5));
            return I1 / Sqrt(2);
        }

        /// <summary>
        /// Coeff for f(x)=sqrt(1+x), if -1 <= x <= 0, and 1, if 0 <= x <= 1
        /// by Tcheb polynomial of the second kind.
        /// </summary>
        /// <param name="k"></param>
        /// <returns></returns>
        public static double Coeff(int k)
        {
            var I2 = k > 0 ? Sin(k * PI / 2) * (k + 1) / k / (k + 2) : PI / 4;
            //var I2 = Integrals.Trapezoid(t => Sin((k + 1) * t) * Sin(t), PI / 2, PI, 10000);
            //var I1 = Integrals.Trapezoid(t => Sqrt(1 + Cos(t)) * Sin((k + 1) * t) * Sin(t), PI / 2, PI, 10000);
            //var I1 = Integrals.Trapezoid(t => Sqrt(2) * Cos(t/2) * Sin((k + 1) * t) * Sin(t), PI / 2, PI, 10000);

            return (I1_opt(k) + I2) * Sqrt(2 / PI);
        }

        public static double CoeffVer1(int k)
        {
            var I1 = Integrals.Trapezoid(t => Sqrt(1 + t) * Sin((k + 1) * Acos(t)), -1, 0, 10000);
            var I2 = Integrals.Trapezoid(t => Sin((k + 1) * Acos(t)), -1, 0, 10000);
            return (I1 + I2) * Sqrt(2 / PI);
        }

        public static double UchebAtOne(int k)
        {
            return Sqrt(2 / PI) * (k + 1);
        }

        public static double PartSumAtOne(int n)
        {
            var sum = 0d;
            for (var k = 0; k <= n; k++)
            {
                sum += Coeff(k) * UchebAtOne(k);
            }
            return sum;
        }

    }
}
