﻿namespace ZorschikovExample
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var rn = new List<string>();
            for (int i = 0; i <= 10000; i++)
            {
                //rn.Add($"{i};{Math.Abs(1 - FourierSeries.PartSumAtOne(i)):F10};{0.01 /  i};{0.03 / i};");
                rn.Add($"{Math.Abs(1 - FourierSeries.PartSumAtOne(i)):F10}");
            }
            File.WriteAllLines("rn.csv", rn);
        }
    }
}
